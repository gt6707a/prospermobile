/**
 * Created with JetBrains WebStorm.
 * User: michael.cheng
 * Date: 10/29/15
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
describe("NoteStatus filter", function() {
    var $filter;

    beforeEach(module("prosperFilters"))     ;

    beforeEach(inject(function(_$filter_) {
        $filter = _$filter_;
    }));

    it("returns correct textual status when given status code", function() {
        var filter = $filter("NoteStatus");
        expect(filter(1)).toEqual("Current");
    });
});
