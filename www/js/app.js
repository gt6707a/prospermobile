var prosperApp = angular.module('prosper', [
    'ionic',
    "prosperServices",
    "prosperControllers",
    "prosperFilters"
]).constant("azureAppKey", "aVqSeUtgaBlOrUJnQrKAvaCzlQpcYX16");

prosperApp.config(["$stateProvider", "$urlRouterProvider", "$httpProvider",
    function($stateProvider, $urlRouterProvider, $httpProvider){

    $stateProvider
        .state('main', {
            url: "",
            abstract: true,
            templateUrl: "main-menu.html"
        })
        .state("main.login", {
            url: "/login",
            views:{
                "menuContent": {
                    templateUrl: function(){
                        return "templates/login.html";
                    },
                    controller: "loginCtrl"
                }
            }
        })
        .state('main.account', {
            url: "/account",
            views: {
                'menuContent' :{
                    templateUrl: function (stateParams){
                        return "templates/account.html";
                    },
                    controller: "accountCtrl"
                }
            }
        }).state('main.notes', {
            url: "/notes/:filters",
            views: {
                'menuContent' :{
                    templateUrl: function(){
                        return "templates/notes.html";
                    },
                    controller: "notesCtrl"
                },
                "subMenuContent": {
                    templateUrl: function() {
                        return "templates/notesMenu.html";
                    },
                    controller: "notesMenuCtrl"
                }
            },
            controller: "notesCtrl"
        }).state('main.note', {
            url: "/note/:id",
            views: {
                'menuContent' :{
                    templateUrl: function(){
                        return "templates/note.html";
                    },
                    controller: "noteCtrl"
                }
            },
            controller: "noteCtrl"
        }).state('main.listings', {
            url: "/listings/:filters",
            views: {
                'menuContent' :{
                    templateUrl: "templates/listings.html",
                    controller: "listingsCtrl"
                },
                "subMenuContent": {
                    templateUrl: "templates/listingsMenu.html",
                    controller: "listingsMenuCtrl"
                }
            },
            controller: "listingsCtrl"
        }).state('main.listing', {
            url: "/listing/:id",
            views: {
                'menuContent' :{
                    templateUrl: function(){
                        return "templates/listing.html";
                    },
                    controller: "listingCtrl"
                }
            },
            controller: "listingCtrl"
        });

    $urlRouterProvider.otherwise("/login");
}]);

prosperApp.run(function($ionicPlatform, $http, azureAppKey) {
    $http.defaults.headers.common["Access-Control-Allow-Origin"]= "*";
    $http.defaults.headers.common["X-ZUMO-APPLICATION"]= azureAppKey;

    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
});