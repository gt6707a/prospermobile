/**
 * Created with JetBrains WebStorm.
 * User: micha_000
 * Date: 4/6/14
 * Time: 11:18 PM
 * To change this template use File | Settings | File Templates.
 */

'use strict';

/* Services */

var prosperServices = angular.module('prosperServices', ['ngResource']);

prosperServices.factory('Account', ['$resource', "Repository",
    function($resource, Repository, commonHTTP){
        var user = Repository.get("login").user;

        return $resource("https://eo-prosper1.azure-mobile.net/api/account",
            {}, {
                get: {
                    method:'POST',
                    isArray: false,
                    responseType: "json",
                    transformRequest: function(data) {
                        data.user = Repository.get("login").user;
                        return angular.toJson(data);
                    }
                }
            });
    }]);

prosperServices.factory('Notes', ['$resource', "Repository",
    function($resource, Repository){
        return $resource("https://eo-prosper1.azure-mobile.net/api/notes", {}, {
            query: {method:'POST', isArray: true, responseType: "json",
                transformRequest: function(data) {
                    data.user = Repository.get("login").user;
                    return angular.toJson(data);
                }}
        });
    }]);

prosperServices.factory('Loans', ['$resource', "Repository",
    function($resource, Repository){
        return $resource("https://eo-prosper1.azure-mobile.net/api/loans", {}, {
            query: {method:'POST', isArray: true, responseType: "json",
                transformRequest: function(data) {
                    data.user = Repository.get("login").user;
                    return angular.toJson(data);
                }}
        });
    }]);

prosperServices.factory('Listings', ['$resource', "Repository",
    function($resource, Repository){
        return $resource("https://eo-prosper1.azure-mobile.net/api/listings", {}, {
            query: {method:'POST', isArray: true, responseType: "json",
                transformRequest: function(data) {
                    data.user = Repository.get("login").user;
                    return angular.toJson(data);
                }}
        });
    }]);

prosperServices.factory('HistoricalListings', ['$resource', "Repository",
    function($resource, Repository){
        return $resource("https://eo-prosper1.azure-mobile.net/api/historicallistings", {}, {
            query: {method:'POST', isArray: true, responseType: "json",
                transformRequest: function(data) {
                    data.user = Repository.get("login").user;
                    return angular.toJson(data);
                }}
        });
    }]);

prosperServices.factory("Repository", [
    function() {
        return {
            get: function(key) {
                var value = window.localStorage[key];
                if (angular.isDefined(value)) {
                    return JSON.parse(window.localStorage[key]);
                }
                return undefined;
            },
            save: function(key, value) {
                window.localStorage[key] = JSON.stringify(value);
            },
            delete: function(key) {
                window.localStorage.removeItem(key);
            }
        };
    }]);

prosperServices.factory('commonHTTP', function() {
    return {
        apiHeaders: {
            'X-ZUMO-APPLICATION': "aVqSeUtgaBlOrUJnQrKAvaCzlQpcYX16"
        }
    };
})  ;