/**
 * Created with JetBrains WebStorm.
 * User: micha_000
 * Date: 4/10/14
 * Time: 12:27 PM
 * To change this template use File | Settings | File Templates.
 */
'use strict';

/* Filters */

angular.module('prosperFilters', [])
.filter('noteStatus', function() {
    return function(status) {
        switch (status)
        {
            case 1:
                return "Current";
            case 2:
                return "Charge Off";
            case 3:
                return "Defaulted";
            case 4:
                return "Completed";
            case 5:
                return "Final Payment In Progress";
            case 6:
                return "Cancelled";
            default:
                return "";
        }
    };
}).filter("noteStatusTextToCode", function() {
    return function(status) {
        switch(status) {
            case "current": return 1;
            case "chargeoff": return 2;
            case "defaulted": return 3;
            case "completed": return 4;
            case "inFinalPayment": return 5;
            case "cancelled": return 6;
            default: return -1;
        }
    };
}).filter('noteStatusClass', function() {
    return function(status) {
        switch (status)
        {
            case 1:
                return "dark";
            case 2:
                return "assertive";
            case 3:
                return "assertive";
            case 4:
                return "balanced";
            case 5:
                return "positive";
            default:
                return "stable";
        }
    };
}).filter('incomeRange', function() {
        return function(incomeRange) {
            switch (incomeRange)
            {
                case 1:
                    return "no income";
                case 2:
                    return "$1 - $24,999";
                case 3:
                    return "$25,000 - $49,000";
                case 4:
                    return "$50,000 - $74,999";
                case 5:
                    return "$75,000 - $99,999";
                case 6:
                    return "$100,000+";
                default:
                    return "Not employed";
            }
        };
}).filter("listingTypeToImgSrc", function() {
        return function(listingType){
            var img = "";
            switch (listingType.toUpperCase())
            {
                case "DEBT CONSOLIDATION":
                    img = "p2p_debt3_thumb.png";
                    break;
                case "AUTO":
                    img = "p2p_auto1_thumb.png";
                    break;
                case "HOME IMPROVEMENT":
                    img = "p2p_home2_thumb.png";
                    break
                case "TAXES":
                    img = "p2p_tax2_thumb.png";
                    break;
                case "BUSINESS":
                    img = "p2p_business1_thumb.png";
                    break;
                case "OTHER":
                    img = "p2p_other4_thumb.png";
                    break;
                case "HOUSEHOLD EXPENSES":
                    img = "p2p_household1_thumb.png";
                    break;
                case "MEDICAL/DENTAL":
                    img = "p2p_medical5_thumb.png";
                    break;
                case "VACATION":
                    img = "p2p_vacation2_thumb.png";
                    break;
                default:
                    img = "p2p_other4_thumb.png";
                    break;
            }

            return "https://images.prosper.com/listing/defaults/3/" + img;
        };
    });
