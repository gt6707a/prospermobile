/**
 * Created with JetBrains WebStorm.
 * User: micha_000
 * Date: 4/6/14
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */

var prosperControllers = angular.module("prosperControllers", []);

prosperControllers.controller("mainCtrl", ["$scope", "$http", "$state", "$ionicLoading", "$ionicSideMenuDelegate", "$ionicPopup",
    function($scope, $http, $state, $ionicLoading, $ionicSideMenuDelegate, $ionicPopup){
//        $http.interceptors.push(function($q) {
//            return {
//                "responseError": function(rejection) {
//                    if (rejection.status === "401") {
//                        // show error and go to login
//                        $state.go("main.login");
//                    }
//                    return $q.reject(rejection);
//                }
//            };
//        });

        $scope.showMenuToggle = false;
        $scope.enableSubMenu = false;

        $scope.$on("$stateChangeSuccess", function(event, toState){
            $scope.showMenuToggle = toState.name !== "main.login";
            $scope.enableSubMenu = false;
            $ionicSideMenuDelegate.toggleRight(false);
        });

        $scope.$on("enableSubMenu", function(name, args){
            $scope.enableSubMenu = args;
        });

        // Should use eventing
        $scope.showLoading = function(){
            $scope.loading = $ionicLoading.show({
               content: "Loading"
            });
        };

        $scope.hideLoading = function() {
            $ionicLoading.hide();
        };

        $scope.username = $scope.password = "";
    }]);

prosperControllers.controller("accountCtrl", ["$scope", "$http", "Account", "Repository",
    function($scope, $http, Account, Repository){

        if (!$scope.isInitialized){
            $scope.showLoading();
            $scope.account = Account.get({}, function(){
                $scope.hideLoading();
                $scope.isInitialized = true;
            }, function(e){
                         $window.alert("Login failed: " + e);
            })  ;
        }
    }]);

prosperControllers.controller("notesMenuCtrl", ["$scope", "$state", "$ionicSideMenuDelegate", "$stateParams", "noteStatusTextToCodeFilter",
    function($scope, $state, $ionicSideMenuDelegate, $stateParams, noteStatusTextToCodeFilter){
        $scope.$on('$stateChangeSuccess', function() {
            if ($state.is("main.notes")) {
                $scope.$emit("enableSubMenu", true);
            }
        })  ;

        function paramHasStatusFilter(status) {
            var condition = "NoteStatus eq " + noteStatusTextToCodeFilter(status);
            return !angular.isUndefined($stateParams.filters) && $stateParams.filters.indexOf(condition) > -1 ? condition : "";
        }

        $scope.filters = {
            "current": paramHasStatusFilter("current"),
            "chargeoff": paramHasStatusFilter("chargeoff"),
            "defaulted": paramHasStatusFilter("defaulted"),
            "completed": paramHasStatusFilter("completed"),
            "inFinalPayment": paramHasStatusFilter("inFinalPayment"),
            "cancelled": paramHasStatusFilter("cancelled")
        };

        $scope.filtersToString = function() {
            var filters = $scope.filters;
            var retval = "";
            for(key in filters) {
                if (filters[key] !== false && filters[key] !== ""){
                    retval = retval + filters[key] + " or ";
                }
            }
            retval = retval.substring(0, retval.length - 4);
            return retval;
        };

        $scope.apply = function() {
            var filters = $scope.filtersToString();
            $state.go("main.notes", { filters: filters}, {
              reload: true
            });
        };

        $scope.cancel = function() {
            // Revert changes. Does angular have a way to revert to original values?
            $ionicSideMenuDelegate.toggleRight(false);
        };
    }]);

prosperControllers.controller("notesCtrl", ["$scope", "Notes", "$stateParams", "$ionicSideMenuDelegate",
    function($scope, Notes, $stateParams, $ionicSideMenuDelegate) {

        var index = 0;
        $scope.hasMore = true;

        $scope.submenu = function() {
            $ionicSideMenuDelegate.toggleRight();
        }

        $scope.query = function() {
            if (!$scope.notes)
                $scope.notes = [];

            if (index === 0)
                $scope.notes.length = 0;

            var params = { "$top": 20, "$skip": index * 20 };
            if ($stateParams.filters !== "") {
                params["$filter"] = $stateParams.filters;
            };

            params["$select"] = "LoanNoteID,NoteStatus,PrincipalBalance,BorrowerRate,InterestPaid";
            // Need to explicitly pass params (for QS) and {} for payload
            Notes.query(params, {}, function(r){
                $scope.hasMore = r.length >= 20;
                index++;
                $scope.notes.push.apply($scope.notes, r);
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.refresh = function() {
            index = 0;
            $scope.notes.length = 0;
            $scope.query();
        };
    }]);

prosperControllers.controller("noteCtrl", ["$scope", "Notes", "Listings", "HistoricalListings", "Loans", "$state", "$q",
    function($scope, Notes, Listings, HistoricalListings, Loans, $state, $q) {
        $scope.id = $state.params.id;
        $scope.showLoading();

        Notes.query({ $filter: "LoanNoteID eq '" + $scope.id + "'"}, {}, function(resp) {
            $scope.note = resp[0];
            var pListing = Listings.query({ $filter: "ListingNumber eq " + $scope.note.ListingNumber }, {}).$promise;
            var pLoan = Loans.query({ $filter: "LoanNumber eq " + $scope.note.LoanNumber }, {}).$promise;
            var pHistoricalListing = HistoricalListings.query({ $filter: "ListingNumber eq " + $scope.note.ListingNumber }, {}).$promise;
            $q.all([ pListing, pHistoricalListing, pLoan ]).then(function(data) {
                $scope.listing = data[0].length === 0 ? data[1][0] : data[0][0];
                $scope.loan = data[2][0];
                $scope.hideLoading();
            });
        });
    }]);

prosperControllers.controller("listingsCtrl", ["$scope", "Listings", "$stateParams",
    // Could abstract some of the same list handling??
    function($scope, Listings, $stateParams) {
        var index = 0;
        $scope.hasMore = true;

        if (!angular.isDefined($stateParams.filters) || $stateParams.filters == null) {
            $stateParams.filters = {};
        }
        $stateParams.filters.ListingStatus = 2;

        $scope.query = function() {
            if (!$scope.listings) {
                $scope.listings = [];
            }

            var params = {"$top":20, "$skip": index * 20};
            if ($stateParams.filters !== "") {
                params["$filter"] = $stateParams.filters;
            };

            //params["$select"] = "LoanNoteID,NoteStatus,PrincipalBalance,BorrowerRate,InterestPaid";
            // Need to explicitly pass params (for QS) and {} for payload
            Listings.query(params, {}, function(r){
                $scope.hasMore = r.length >= 20;
                index++;
                $scope.listings.push.apply($scope.listings, r);
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.refresh = function() {
            index = 0;
            $scope.listings.length = 0;
            $scope.query();
        };
    }]);

prosperControllers.controller("listingsMenuCtrl", ["$scope", "$state", "$ionicSideMenuDelegate", "$stateParams",
    function($scope, $state, $ionicSideMenuDelegate, $stateParams){

        $scope.$on('$stateChangeSuccess', function() {
            if ($state.is("main.listings")) {
                $scope.$emit("enableSubMenu", true);
            }
        });

        $scope.filters = { prosperScore: 5 };
        $scope.filters.categories = [
            { title: "Not Available", value: 0, isSet: false },
            { title: "Debt Consolidation", value: 1, isSet: false },
            { title: "Home Improvement", value: 2, isSet: false },
            { title: "Business", value: 3, isSet: false },
            { title: "Auto", value: 6, isSet: false },
            { title: "Other", value: 7, isSet: false },
            { title: "Baby Adoption", value: 8, isSet: false },
            { title: "Engagement Ring Purchase", value: 11, isSet: false },
            { title: "Large Purchases", value: 14, isSet: false },
            { title: "Medical/Dental", value: 15, isSet: false },
            { title: "RV", value: 17, isSet: false },
            { title: "Taxes", value: 18, isSet: false }
        ];

        // Apply filters when opening
        $scope.$watch(function() { return $ionicSideMenuDelegate.getOpenRatio(); }, function(value) {
            if (value === -1) {
                angular.forEach($scope.filters.categories, function(cat, key) {
                    var condition = "ListingCategory eq " + cat.value;
                    cat.isSet = !angular.isUndefined($stateParams.filters) && $stateParams.filters.indexOf(condition) > -1;
                });
            }
        });

        function filtersToString () {
            var retval = "";
            angular.forEach($scope.filters.categories, function(cat, key) {
                retval = retval + (cat.isSet ? ("ListingCategory eq " + cat.value + " or ") : "");
            });

            retval = retval.substring(0, retval.length - 4);
            retval = "ProsperScore ge " + $scope.filters.prosperScore +
                (retval == "" ? "" : (" and (" + retval + ")"));
            return retval;
        };

        $scope.apply = function() {
            var filters = filtersToString();
            $state.go("main.listings", { filters: filters}, {
                reload: true
            });
        };

        $scope.cancel = function() {
            // Revert changes. Does angular have a way to revert to original values?
            $ionicSideMenuDelegate.toggleRight(false);
        };
    }]);

prosperControllers.controller("listingCtrl", ["$scope", "Listings", "$state", "$ionicPopup", "$window",
    function($scope, Listings, $state, $ionicPopup, $window) {
        $scope.id = $state.params.id;
        $scope.amountToInvest = 0;
        $scope.showLoading();

        Listings.query({ $filter: "ListingNumber eq " + $scope.id}, {}, function(resp) {
            $scope.listing = resp[0];
            $scope.hideLoading();
        });

        $scope.invest = function() {
            $scope.amountToInvest = 0;
            var investPopup = $ionicPopup.show({
               template: "$<input type='number' ng-model='amountToInvest'>",
                title: "Enter amount to invest",
                scope: $scope,
                buttons: [
                    { text: "Cancel" },
                    {
                        text: "<b>Invest</b>",
                        type: "button-positive",
                        onTap: function(e) {
                            $window.alert("Not making an actual API request to invest because it's real $$. Sorry. But assume it could.");
                        }
                    }
                ]
            });
        };
    }]);

prosperControllers.controller("loginCtrl", ["$scope", "$state", "Repository", "$ionicPopup",
    function($scope, $state, Repository, $ionicPopup){
        $ionicPopup.show({
            template: "This is a demo of AngularJS + Ionic mobile client. " +
                "It is best viewed on mobile browser or can be installed as a native app. " +
                "It focuses on implementing working functionality using fundamental AngularJS components such modules, controllers, filters, and services, etc. " +
                "The app shows live data by querying API hosted on Azure. Simply tap Sign In to begin.",
            title: "Hello",
            buttons: [ { text: "Ok" }]
        });

        var login = Repository.get("login");
        if (!angular.isDefined(login) || !login.rememberMe) {
            $scope.login = { user: { u: "test", p: "test" }, rememberMe: false };
            Repository.delete("login");
        } else {
            $scope.login = login;
        }

        $scope.signIn = function() {
            if ($scope.login.u !== "" && $scope.login.p !== ""){
                Repository.save("login", $scope.login);
                $state.go("main.account");
            }

            // Show error popups
        };
}]);
